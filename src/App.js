import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import localStorage from "local-storage";
import './App.css';


function Ratio(props) {
  return (
    <div>
      <h2>Brew Ratio</h2>
      <p>How many grams of ground coffee do you want to use per liter of water? Changing this will change the <i>strength</i> of the coffee.</p>
      <p><i>Tip: For a V60 60 of coffee per liter (1000g) of water is a good starting point.</i></p>
      <p>
        <input
          type="number"
          value={props.ratio}
          onChange={(event) => props.onChange(event.target.value)}
        />
        <span> g/L</span>
      </p>
    </div>
  )
}


class Recipe extends React.Component {
  constructor(props) {
    super(props)
    let setting_water = localStorage("setting_water");
    if (setting_water === null) {
      setting_water = true;
    }
    this.state = {
      setting_water: setting_water,
    };
  }

  update(value) {
    let coffee;
    if (this.state.setting_water) {
      coffee = value * this.props.ratio / 1000;
    } else {
      coffee = value;
    }
    this.props.onChange(coffee);
  }

  get water() {
    return (this.props.coffee / this.props.ratio * 1000).toFixed(0);
  }


  render() {
    localStorage("setting_water", this.state.setting_water);
    return (
      <div>
        <h2>Recipe</h2>
        <p>How much water and ground coffee do you want to use?</p>
        <p><i>Tip: If you only a small amount of a coffee left you can enter that and it will tell you how much water you need to use.</i></p>
        <input
          type="number"
          value={this.state.setting_water ? this.water : this.props.coffee}
          onChange={(event) => this.update(event.target.value)}
        />
        <span> g of </span>
        <select onChange={(event) => this.setState({ setting_water: event.target.value === "water" })} value={this.state.setting_water ? "water" : "coffee"}>
          <option value="coffee">coffee</option>
          <option value="water">water</option>
        </select>
        <span> and {this.state.setting_water ? this.props.coffee : this.water}g of {!this.state.setting_water ? "water" : "coffee"}.</span>

      </div>)
  }
}
function Method(props) {
  return (
    <div>
      <h1>Method</h1>
      <ol className="method">
        <h2>Prep</h2>
        <li>Grind {(props.coffee).toFixed(1)}g of coffee.</li>
        <li>Rinse the filter paper with warm/hot water.</li>
        <li>Add the coffee to the V60.</li>
        <li>Make a small well in the center of the grounds.</li>
        <h2>Bloom</h2>
        <h3>t = 0s</h3>
        <li>Start the timer.</li>
        <li>Add {(props.coffee * 2).toFixed(1)}g of water to saturate the coffee. If more is needed you can use up to {(props.coffee * 3).toFixed(1)}g of water.</li>
        <li>Swirl the coffee until is evenly mixed/not lumpy anymore.</li>
        <h2>The pour</h2>
        <h3>t = 45s</h3>
        <li>Add water gradualy so that there is a total of {(props.water * 0.6).toFixed(1)}g by 1m 15s.</li>
        <h3>t = 1m 15s</h3>
        <li>Add water gradualy so that there is a total of {props.water.toFixed(1)}g by 1m 45s.</li>
        <h3>t = 1m 45s</h3>
        <li>Stir the coffee once in each direction to knock grounds off the side.</li>
        <li>When it is safe, give the coffee a swirl to flatten the bed.</li>
        <h2>The drawdown</h2>
        <li>Leave the coffee to drain.</li>
        <li>Once all the coffee has drained into the cup, serve and enjoy :)</li>
      </ol>
    </div>
  )
}


function Nav(props) {
  return (
    <nav>
      <h1><Link to="/" onClick={() => (document.getElementById("menu-input").checked = false)}>V60 Recipe</Link></h1>
      <input type="checkbox" id="menu-input" />
      <label htmlFor="menu-input" id="menu-button">
        <svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none" /><path fill="rgb(242, 204, 143)" d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z" /></svg>
      </label>
      <ul>
        <li><Link to="/setup" onClick={() => (document.getElementById("menu-input").checked = false)}>Setup</Link></li>
        <li><Link to="/method" onClick={() => (document.getElementById("menu-input").checked = false)}>Method</Link></li>
      </ul>
    </nav>
  );
}

function Home() {
  return (
    <div>
      <h1>Welcome</h1>
      <p>
        Welcome to V60 Recipe.
        This webapp allows you to generate a V60 method for a given brew ratio and total brew weight.
        It is inspired <a href="https://www.youtube.com/watch?v=AI4ynXzkSQo">James Hoffmanns Ultimate V60 Technique</a>.
      </p>
    </div>
  );
}

class App extends React.Component {
  constructor(props) {
    super(props);
    let ratio = localStorage("ratio");
    if (ratio === null) {
      ratio = 60;
    }
    let coffee = localStorage("coffee");
    if (coffee === null) {
      coffee = 15;
    }
    this.state = { ratio: ratio, coffee: coffee };
  }
  get water() {
    return this.state.coffee / this.state.ratio * 1000;
  }
  render() {
    localStorage("ratio", this.state.ratio);
    localStorage("coffee", this.state.coffee);
    return (
      <Router>
        <Nav />
        <main>
          <Switch>
            <Route path="/setup">
              <h1>Setup</h1>
              <Ratio
                ratio={this.state.ratio}
                onChange={(ratio) => this.setState({ ratio: ratio })}
              />
              <Recipe
                ratio={this.state.ratio}
                coffee={this.state.coffee}
                onChange={(coffee) => this.setState({ coffee: parseInt(coffee) })}
              />
            </Route>
            <Route path="/method">
              <Method
                coffee={this.state.coffee}
                water={this.water}
              />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </main>
      </Router>
    )
  }
}

export default App;
